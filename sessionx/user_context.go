package sessionx

type UserContext struct {
	TenantId              int64  // 租户编号
	TenantCode            string // 租户唯一编号
	UserId                int64  // 用户唯一编号
	UserCode              string // 用户唯一编码
	UserName              string // 用户的登录名
	Mobile                string // 电话号码
	Mail                  string // 邮箱
	JwtToken              string // jwt token
	AccessToken           string // 请求的token
	AccessTokenExpiredAt  int64  // 访问token 过期时间
	RefreshTokenExpiredAt int64  // 刷新token 过期时间
	PermissionCodes       []string
	PermissionGroup       string // 权限组
	PermissionCode        string // 当前请求的url的操作权限码
	SpanId                string
	TraceId               string
	ClientId              string // 用户使用的Oauth2客户端
}

func (uc UserContext) IsDefaultTenant() bool {
	return uc.TenantId == 1
}

func (uc UserContext) IsSuper() bool {
	return uc.UserId == 1
}
