package mapx

import "testing"

type Demo struct {
	Id   int64
	Name string
}

var data = []Demo{
	{
		Id:   1,
		Name: "d1",
	},
	{
		Id:   2,
		Name: "d2",
	},
	{
		Id:   3,
		Name: "d3",
	},
	{
		Id:   1,
		Name: "d11",
	},
	{
		Id:   2,
		Name: "d22",
	},
}

func TestFromSlice(t *testing.T) {
	m := FromSlice[int64, string, Demo, map[int64][]string](data, func(d Demo) int64 {
		return d.Id
	}, func(d Demo) string {
		return d.Name
	})

	if _, ok := m[2]; !ok {
		t.Fail()
	}
}
