package gormx

import (
	"fmt"

	"gorm.io/gorm"
)

// 使用deleted 标记字段排除删除内容
func ExcludeDeleted(db *gorm.DB) *gorm.DB {
	return db.Where("deleted = 0 ")
}

// 使用deleted字段排除删除内容并指定表名
func ExcludeDeletedScope(alias string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where(fmt.Sprintf("%s.deleted = 0", alias))
	}
}
