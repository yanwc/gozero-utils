package utils

import "sync"

type StringSet struct {
	sync.RWMutex
	m map[string]bool
}

func NewStringSet(items ...string) *StringSet {
	s := &StringSet{
		m: make(map[string]bool, len(items)),
	}
	s.Add(items...)
	return s
}

func (s *StringSet) Add(items ...string) {
	s.Lock()
	defer s.Unlock()
	for _, v := range items {
		s.m[v] = true
	}
}

func (s *StringSet) Remove(items ...string) {
	s.Lock()
	defer s.Unlock()
	for _, v := range items {
		delete(s.m, v)
	}
}

func (s *StringSet) Has(items ...string) bool {
	s.RLock()
	defer s.RUnlock()
	for _, v := range items {
		if _, ok := s.m[v]; !ok {
			return false
		}
	}
	return true
}

func (s *StringSet) Count() int {
	return len(s.m)
}

func (s *StringSet) Clear() {
	s.Lock()
	defer s.Unlock()
	s.m = map[string]bool{}
}

func (s *StringSet) Empty() bool {
	return len(s.m) == 0
}

func (s *StringSet) ToArray() []string {
	s.RLock()
	defer s.RUnlock()
	list := make([]string, 0, len(s.m))
	for item := range s.m {
		list = append(list, item)
	}
	return list
}

// Union 并
func (s *StringSet) Union(sets ...*StringSet) *StringSet {
	r := NewStringSet(s.ToArray()...)
	for _, set := range sets {
		for e := range set.m {
			r.m[e] = true
		}
	}
	return r
}

// Intersect 交
func (s *StringSet) Intersect(sets ...*StringSet) *StringSet {
	r := NewStringSet(s.ToArray()...)
	for _, set := range sets {
		for e := range s.m {
			if _, ok := set.m[e]; !ok {
				delete(r.m, e)
			}
		}
	}
	return r
}

// Minus 差
func (s *StringSet) Minus(sets ...*StringSet) *StringSet {
	r := NewStringSet(s.ToArray()...)
	for _, set := range sets {
		for e := range set.m {
			if _, ok := s.m[e]; ok {
				delete(r.m, e)
			}
		}
	}
	return r
}

// Complement 补
func (s *StringSet) Complement(full *StringSet) *StringSet {
	r := NewStringSet()
	for e := range full.m {
		if _, ok := s.m[e]; !ok {
			r.Add(e)
		}
	}
	return r
}
