package errx

type ErrorOption func(*Error)

func WithCodeOption(code Code) ErrorOption {
	return func(ce *Error) {
		ce.code = code
	}
}

func WithMsgOption(msg string) ErrorOption {
	return func(ce *Error) {
		ce.msg = msg
	}
}

func WithErrorOption(err error) ErrorOption {
	return func(ce *Error) {
		ce.inner = err
	}
}
