package typex

import (
	"fmt"
	"testing"
)

type Test struct {
	V1 int
}

func TestValueToPtr(t *testing.T) {
	x := Test{V1: 200}
	a := ValueToPtr(x.V1)
	fmt.Println(*a)
}
