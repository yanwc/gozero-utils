package utils

import "testing"

func TestRandString(t *testing.T) {
	s := RandString(10)
	t.Error(len(s) == 10)
}

func TestRandInt(t *testing.T) {
	s := RandInt(5)
	t.Error(len(s) == 5)
}
