package httpx

import (
	"context"
	"fmt"
	"net/http"
	"testing"
)

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1aGsiLCJkYXRhIjp7InVzZXJJZCI6MSwiYWNjZXNzVG9rZW4iOiIyNzNjNGFlYy00MjNkLTQzNGQtODE0My02MjIzODlhNDVjMTQiLCJ0ZW5hbnRJZCI6MSwidXNlclR5cGUiOiJhZG1pbiJ9LCJleHAiOjM0NjEzNzkzMDgsImlzcyI6InVoayIsInN1YiI6InVoay1zeXN0ZW0ifQ.b_GFbISmk6Z3md-Kq7EPOOWN6WKPcb9CEBMwdE63R_k"

func TestGetTokenFromRequest(t *testing.T) {
	str, err := GetTokenFromRequest(&http.Request{
		Header: http.Header{
			"Authorization": []string{fmt.Sprintf("Bearer %s", token)},
		},
	})
	if err != nil {
		t.Error(err)
	}

	if str != token {
		t.Fail()
	}
}

func TestSetTokenContext(t *testing.T) {
	ctx := context.Background()
	ctx1 := context.Background()
	r := &http.Request{
		Header: http.Header{
			"Authorization": []string{fmt.Sprintf("Bearer %s", token)},
		},
	}
	r = r.WithContext(ctx1)
	rn, tt, err := WithJwtTokenContext(ctx, r, "#222#ABsse@")
	if err != nil {
		t.Error(err)
	}

	d, e := GetJwtTokenDataFromCtx(rn.Context())
	if e != nil {
		t.Error(e)
	}

	if d.TenantId != 1 {
		t.Fail()
	}

	if token != tt {
		t.Fail()
	}
}
