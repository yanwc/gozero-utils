package utils

import "math/rand"

var (
	letterRunes    = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	letterAnyChars = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789!@#$%^&")
)

// RandStringRunes 随机字符串
func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func RandSecretString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterAnyChars[rand.Intn(len(letterAnyChars))]
	}
	return string(b)
}
