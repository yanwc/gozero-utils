package jsonx

import (
	"encoding/json"
	"testing"
)

type Test1 struct {
	Name    string
	Address string
}

func TestStringToSlice(t *testing.T) {
	data := []Test1{
		{
			Name:    "test1",
			Address: "address1",
		},
		{
			Name:    "test2",
			Address: "address2",
		},
	}

	str, err := json.Marshal(data)
	if err != nil {
		t.Fail()
	}

	s, err := StringToSlice[Test1](string(str))
	if err != nil {
		t.Fail()
	}

	if len(s) != len(data) {
		t.Fail()
	}
}
