package mapx

func GetZeroValue[T any](data map[string]*T, key string, defaultValue T) T {
	v, ok := data[key]
	if ok {
		return *v
	} else {
		return defaultValue
	}
}

func FromSlice[K comparable, V any, T any, R map[K][]V](data []T, fk func(T) K, fv func(T) V) R {
	r := make(R)
	for _, d := range data {
		if _, ok := r[fk(d)]; ok {
			r[fk(d)] = append(r[fk(d)], fv(d))
		} else {
			r[fk(d)] = []V{fv(d)}
		}
	}

	return r
}
