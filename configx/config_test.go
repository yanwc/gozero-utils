package configx

import (
	"fmt"
	"testing"
	"time"
)

func TestConfigStringType(t *testing.T) {
	x, err := StringType.Value("ok")
	if err != nil {
		t.Error(err)
	}

	if x != "ok" {
		t.Fail()
	}
}

func TestConfigIntType(t *testing.T) {
	v, err := IntType.Value("100")
	if err != nil {
		t.Error(err)
	}

	if v != 100 {
		t.Fail()
	}
}

func TestConfigBoolType(t *testing.T) {
	v, err := BoolType.Value("false")
	if err != nil {
		t.Error(err)
	}

	if v.(bool) {
		t.Fail()
	}
}

func TestDateType(t *testing.T) {
	v, err := DateType.Value("1728633485")
	if err != nil {
		t.Error(err)
	}

	if !v.(time.Time).Equal(time.Unix(int64(1728633485), 0)) {
		t.Fail()
	}
}

func TestDateRangeType(t *testing.T) {
	v, err := DateRangeType.Value("[1728633485,1728633485)")
	if err != nil {
		t.Error(err)
	}

	if !time.Unix(int64(1728633485), 0).Equal(v.(RangeDateValue).Start) {
		t.Fail()
	}

	if !time.Unix(int64(1728633485), 0).Equal(v.(RangeDateValue).End) {
		t.Fail()
	}
}

func TestNumberType(t *testing.T) {
	v, err := NumberType.Value("0.11")
	if err != nil {
		t.Error(err)
	}

	if v != 0.11 {
		t.Fail()
	}
}

func TestNumberRangeType(t *testing.T) {
	v, err := NumberRangeType.Value("[12.3453,34.2243]")
	if err != nil {
		t.Error(err)
	}

	if v.(RangeFloat64Value).Start != float64(12.3453) {
		t.Fail()
	}

	if v.(RangeFloat64Value).End != float64(34.2243) {
		t.Fail()
	}
}

func TestDictStringArrayType_String(t *testing.T) {
	v, err := DictStringArrayType.Value("[\"1\",\"2\",\"3\"]")
	if err != nil {
		t.Error(err)
	}

	if s, ok := v.([]string); !ok {
		t.Fail()
		fmt.Println(s)
	}
}

func TestDictFloat64ArrayType(t *testing.T) {
	v, err := DictNumberArrayType.Value("[1,2,3,4]")
	if err != nil {
		t.Error(err)
	}

	if s, ok := v.([]float64); !ok {
		t.Fail()
		fmt.Println(s)
	}
}
