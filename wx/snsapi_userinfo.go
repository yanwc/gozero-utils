package wx

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/zeromicro/go-zero/core/logx"
)

type SnsApiWxUserInfo struct {
	OpenId     string `json:"openid"`
	NickName   string `json:"nickname"`
	Sex        int64  `json:"sex"`
	Province   string `json:"province"`
	City       string `json:"city"`
	Country    string `json:"country"`
	HeadImgUrl string `json:"headimgurl"`
	Privilege  string `json:"privilege"`
	UnionId    string `json:"unionid"`
}

func SnsApiUserInfo(accessToken string, openId string) (*SnsApiWxUserInfo, error) {
	url := fmt.Sprintf("https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN", accessToken, openId)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	logx.Info(string(body))
	var u SnsApiWxUserInfo
	err = json.Unmarshal(body, &u)
	if err != nil {
		return nil, err
	}

	return &u, nil
}
