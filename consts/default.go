package consts

const (
	// 默认租户编号
	DefaultTenantId = 1
	// 默认租户角色编号
	DefaultTenantRoleId = 1
	// 默认租户套餐编号
	DefaultTenantPackageId = 1
	// 默认租户的默认用户编号
	DefaultTenantUserId = 1
)
