package cachex

import (
	"database/sql"

	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/syncx"
)

var stats = cache.NewStat("cachex")

// can't use one SingleFlight per conn, because multiple conns may share the same cache key.
var singleFlights = syncx.NewSingleFlight()

func NewCache(c cache.CacheConf, opts ...cache.Option) cache.Cache {
	return cache.New(c, singleFlights, stats, sql.ErrNoRows, opts...)
}
