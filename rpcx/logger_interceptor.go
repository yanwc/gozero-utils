package rpcx

import (
	"context"

	"gitee.com/yanwc/gozero-utils/errx"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"google.golang.org/grpc/status"
)

func LoggerInterceptor(ctx context.Context, req interface{}, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	resp, err = handler(ctx, req)
	if err != nil {
		causeErr := errors.Cause(err)              // err类型
		if e, ok := causeErr.(*errx.Error); ok { // 自定义错误类型
			logx.WithContext(ctx).Errorf("RPC error code :%d msg: %s  inner error %v ", e.GetErrCode(), e.GetMsg(), e.InnerError())
			// 转成grpc err
			err = status.Error(codes.Code(e.GetErrCode()), e.GetMsg())
		} else {
			logx.WithContext(ctx).Errorf("RPC %+v", err)
		}
	}

	return resp, err
}
