package gormx

import (
	"fmt"

	"gorm.io/gorm"
)

func Enabled(enabled *bool) func(tx *gorm.DB) *gorm.DB {
	return func(tx *gorm.DB) *gorm.DB {
		if enabled != nil {
			tx.Where("enabled = ?", enabled)
		}

		return tx
	}
}

// alias 表别名
func EnabledScope(alias string, enabled *bool) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if enabled != nil {
			return db.Where(fmt.Sprintf("%s.enabled = ?", alias), enabled)
		}

		return db
	}
}
