package defaultvalue

func ZeroValueDefaultValue[T ~int64 | ~int32 | ~int16 | ~int | ~string](value T, defaultValue T) T {
	switch v := interface{}(value).(type) {
	case string:
		if len(v) == 0 {
			return defaultValue
		} else {
			return value
		}
	case int64:
		if v == 0 {
			return defaultValue
		} else {
			return value
		}
	case int32:
		if v == 0 {
			return defaultValue
		} else {
			return value
		}
	case int16:
		if v == 0 {
			return defaultValue
		} else {
			return value
		}
	case int8:
		if v == 0 {
			return defaultValue
		} else {
			return value
		}
	}

	return value
}

func PointerValueDefaultValue[T ~int64 | ~int32 | ~int16 | ~int | ~string | ~bool](value *T, defaultValue T) T {
	if value == nil {
		return defaultValue
	} else {
		return *value
	}
}
