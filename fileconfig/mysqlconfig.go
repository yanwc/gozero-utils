package fileconfig

// MySql 配置
type MySql struct {
	Host string
	Port int
	Db   string
	User string
	Pwd  string
}
