package api

type ResponseOk struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}



func Success(data interface{}) *ResponseOk {
	return &ResponseOk{
		Success: true,
		Message: "success",
		Data:    data,
	}
}
