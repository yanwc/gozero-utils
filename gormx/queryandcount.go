package gormx

import (
	"gitee.com/yanwc/gozero-utils/errx"
	"gorm.io/gorm"
)

func QueryAndCount[T any](query *gorm.DB, pageIndex, pageSize int) ([]*T, int64, *errx.Error) {
	var rows []*T
	var count int64
	if rev := query.Count(&count); rev.Error != nil {
		return nil, 0, errx.New(errx.DB_QUERY, errx.WithErrorOption(rev.Error), errx.WithMsgOption("查询失败"))
	}

	if rev := query.Scopes(Paginate(pageIndex, pageSize)).Find(&rows); rev.Error != nil {
		return nil, 0, errx.New(errx.DB_QUERY, errx.WithErrorOption(rev.Error), errx.WithMsgOption("查询明细失败"))
	}

	return rows, count, nil
}
