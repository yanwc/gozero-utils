package wx

import (
	"testing"

	"github.com/zeromicro/go-zero/core/stores/redis"
)

func TestTemplateMessageSend(t *testing.T) {
	tms := TemplateMessage{
		templateId:     "aphyMoYjz7rZnxWL2bQfob1dkLxaqSdTbjXALl0Cvwk",
		accessTokenKey: "admin",
		appId:          "wx3cdb96df92afc5be",
		appSecret:      "2ef784c8f2c397b5fb6e023d5427f58e",
	}
	tms.Send("page", "oYIuRs53wCVglB8lEfELw7U6WxaY", Data{
		"orderid": {
			Value: "10000000",
			Color: "#173177",
		},
	}, Developer)
}

func TestTemplateMessageSendWithRedis(t *testing.T) {
	tms := TemplateMessage{
		templateId:     "aphyMoYjz7rZnxWL2bQfob1dkLxaqSdTbjXALl0Cvwk",
		accessTokenKey: "admin",
		appId:          "wx3cdb96df92afc5be",
		appSecret:      "2ef784c8f2c397b5fb6e023d5427f58e",
		redisConf: redis.RedisConf{
			Host: "127.0.0.1:6379",
			Pass: "123456",
		},
	}
	tms.Send("page", "oYIuRs53wCVglB8lEfELw7U6WxaY", Data{
		"orderid": {
			Value: "10000000",
			Color: "#173177",
		},
	}, Developer)
}

func TestUniformMessageRedis(t *testing.T) {
	tms := UniformMessage{
		templateId:     "aphyMoYjz7rZnxWL2bQfob1dkLxaqSdTbjXALl0Cvwk",
		accessTokenKey: "admin",
		appId:          "wx3cdb96df92afc5be",
		appSecret:      "2ef784c8f2c397b5fb6e023d5427f58e",
		redisConf: redis.RedisConf{
			Host: "127.0.0.1:6379",
			Pass: "123456",
		},
	}
	tms.Send("/page/page/index", "formid", "oYIuRs53wCVglB8lEfELw7U6WxaY", Data{
		"thing3": {
			Value: "10000000",
			Color: "#173177",
		},
		"character_string5": {
			Value: "aaaa",
			Color: "",
		},
	})
}
