package rpcx

import (
	"context"

	redis "github.com/redis/go-redis/v9"
)

func RegisterAppTokens(appTokens map[string]string, key string, host string, password string) {
	client := redis.NewClient(&redis.Options{
		Password: password,
		Addr:     host,
	})

	_, err := client.HSet(context.Background(), key, appTokens).Result()
	if err != nil {
		panic(err)
	}
}
