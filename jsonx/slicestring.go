package jsonx

import (
	"encoding/json"

	"github.com/zeromicro/go-zero/core/logx"
)

func SliceMarshalString[T any](data []T) (string, error) {
	if len(data) == 0 {
		return "[]", nil
	}

	d, err := json.Marshal(&data)
	return string(d), err
}

func SliceMarshalStringOrEmpty[T any](data []T) string {
	v, err := SliceMarshalString(data)
	if err != nil {
		logx.Error(err)
		return "[]"
	}

	return v
}
