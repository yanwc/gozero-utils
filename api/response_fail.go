package api

import "gitee.com/yanwc/gozero-utils/errx"

type ResponseFail struct {
	Success bool      `json:"success"`
	Message string    `json:"message"`
	Code    errx.Code `json:"code"`
	Trace   string    `json:"trace"`
	Span    string    `json:"span"`
}

func Fail(errCode errx.Code, errMsg string) *ResponseFail {
	return &ResponseFail{
		Success: false,
		Message: errMsg,
		Code:    errCode,
	}
}

func FailWithTrace(errCode errx.Code, errMsg string, span, trace string) *ResponseFail {
	return &ResponseFail{
		Success: false,
		Message: errMsg,
		Code:    errCode,
		Trace:   trace,
		Span:    span,
	}
}
