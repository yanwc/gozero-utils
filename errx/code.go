package errx

const OK Code = 200

// Code 错误编码 Code:100_00~199_99  100_xx:为预定义错误码
// 规则：业务编码(3位)+功能编码(2位)
type Code uint32

const (
	// 业务错误
	BIZ = Code(100_00)
	// SERVER_COMMON 通用服务编码
	SERVER Code = 100_01
	// REUQEST_PARAM 请求参数错误
	REUQEST_PARAM Code = 100_02
	// TOKEN_EXPIRE Token过期
	TOKEN_EXPIRE Code = 100_03
	// TOKEN_GENERATE Token生成错误
	TOKEN_GENERATE Code = 100_04
	// DB数据库错误
	DB Code = 100_05
	// DB_QUERY 数据库查询错误
	DB_QUERY Code = 100_06
	// DB_LAST_INSERT_ID 获取新书编号错误
	DB_LAST_INSERT_ID Code = 100_07
	// DB_INSERT 数据库创建错误
	DB_INSERT Code = 100_08
	// DB_NOT_FOUND 记录不存在
	DB_NOT_FOUND Code = 100_09
	// DB_ALREADY_EXISTS 记录已经存在
	DB_ALREADY_EXISTS Code = 100_10
	// REQUEST_PARAM_REQUIRED 删除必填
	PARAM_REQUIRED Code = 100_11
	// DB_UPDATE 更新错误
	DB_UPDATE Code = 100_12
	// SQL_BUILD sql构建错误
	SQL_BUILD = Code(100_13)
	// MARSHAL json解析错误
	JSON_MARSHAL = Code(100_14)
	// TOKEN_PARSER Token解析错误
	TOKEN_PARSER = Code(100_15)
	// CACHE_EXPIRE 缓存过期
	CACHE_EXPIRE = Code(100_16)
	// CACHE_ERROR 缓存错误
	CACHE_ERROR = Code(100_17)
	// LOGIN_TIMEOUT 登录超时
	LOGIN_TIMEOUT = Code(100_18)
	// SIGNED_IN_ELSE_WHERE 在其他地方登录
	SIGNED_IN_ELSE_WHERE = Code(100_19)
	// REQUIRE_TOKEN
	REQUIRED_TOKEN = Code(100_20)
	// CAPTCHA 验证码错误
	CAPTCHA_ERROR = Code(100_21)
	// RECORD_DISABLED 禁用
	RECORD_DISABLED = Code(100_22)
	// CACHE_NOT_FOUND 缓存不存在
	CACHE_NOT_FOUND = Code(100_23)
	// DB_DELETE 数据库删除错误
	DB_DELETE    = Code(100_24)
	CACHE_DELETE = Code(100_25)
	// 未授权
	UNAUTHORIZED = Code(100_31)
	// 密码错误
	PASSWORD = Code(100_32)
	// 密码不一致
	PASSWORD_INCONSISTENT = Code(100_33)
	// 未实现
	UNIMPLEMENTED = Code(100_34)
	// 未登录认证
	UNAUTHENTICATION = Code(100_35)
	// 数据库并发更新失败
	DB_CONCURRENT_UPDATE_FAILED = Code(100_36)
	// 系统配置范围格式错误
	CONFIG_RANGE_FORMAT = Code(100_37)
	// 功能未实现
	NOT_IMPLEMENTED = Code(100_38)
	// 功能不支持
	NOT_SUPPORTED = Code(100_39)
	// jwt token 格式错误
	JWT_TOKEN_FORMAT = Code(100_40)
	// token必须
	TOKEN_IS_REQUIRED = Code(100_41)
	// 用户上下文不存在
	USER_CONTEXT_NOT_EXIST = Code(100_42)
	// 唯一编码生成错误
	UUID_GENERATE = Code(100_43)
	// 原密码不正确
	PASSWORD_NEW_OLD_CONSISTENT = Code(100_44)
	// 用户名密码不正确
	PASSWORD_USER = Code(100_45)
	// 反序列失败
	JSON_UNMARSHAL = Code(100_46)
	// 非法转换
	STRING_CONVERT_NUMBER = Code(100_47)

	// token验证
	TOKEN_VALID = Code(100_48)

	// http客户端请求错误
	HTTP_CLIENT_REQUEST_ERROR = Code(100_49)

	// http客户端响应解码错误
	HTTP_CLIENT_DECODE_RESP_ERROR = Code(100_50)

	// http客户端读取body错误
	HTTP_CLIENT_READ_BODY_ERROR = Code(100_51)
)
