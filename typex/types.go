package typex

// ConvertBoolToInt64 bool转整形
func BoolToInt64(value bool) int64 {
	if value {
		return 1
	} else {
		return 0
	}
}

func Int64ToBool(value int64) bool {
	if value > 0 {
		return true
	} else {
		return false
	}
}
