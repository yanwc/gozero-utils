package typex

import (
	"database/sql"
	"time"
)

func PtrBoolToNullBool(value *bool) sql.NullBool {
	if value == nil {
		return sql.NullBool{Valid: false}
	}

	return sql.NullBool{Bool: *value, Valid: true}
}

func BoolToNullBool(v bool) sql.NullBool {
	return sql.NullBool{Valid: true, Bool: v}
}

func NullBoolToPtrBool(v sql.NullBool) *bool {
	if v.Valid {
		return ValueToPtr(v.Bool)
	} else {
		return nil
	}
}

func NullBoolToBool(v sql.NullBool) bool {
	if v.Valid {
		return v.Bool
	} else {
		return false
	}
}

func PtrStrToNullString(value *string) sql.NullString {
	if value == nil {
		return sql.NullString{Valid: false}
	}

	return sql.NullString{String: *value, Valid: true}
}

func StrToNullString(value string) sql.NullString {
	if len(value) == 0 {
		return sql.NullString{}
	}

	return sql.NullString{String: value, Valid: true}
}

func NullStringToStr(value sql.NullString) string {
	if value.Valid {
		return value.String
	} else {
		return ""
	}
}

func NullStringToPtrStr(v sql.NullString) *string {
	if v.Valid {
		return ValueToPtr(v.String)
	} else {
		return nil
	}
}

func PtrIntToNullInt64[I ~int | ~int64 | ~int8 | ~int32 | ~int16](value *I) sql.NullInt64 {
	if value == nil {
		return sql.NullInt64{}
	} else {
		return sql.NullInt64{Int64: int64(*value), Valid: true}
	}
}

func NullInt64ToInt64(value sql.NullInt64) int64 {
	if value.Valid {
		return value.Int64
	} else {
		return 0
	}
}

func NullInt64ToPtrInt64(v sql.NullInt64) *int64 {
	if v.Valid {
		return ValueToPtr(v.Int64)
	} else {
		return nil
	}
}

func PtrUnixToNullTime(t *int64, nsec int64) sql.NullTime {
	if t == nil {
		return sql.NullTime{Valid: false}
	} else {
		return sql.NullTime{Valid: true, Time: time.Unix(*t, nsec)}
	}
}

func NullTimeToPtrUnix(t sql.NullTime) *int64 {
	if !t.Valid {
		return new(int64)
	} else {
		return ValueToPtr(t.Time.Unix())
	}
}

func NullTimeToUnix(value sql.NullTime) int64 {
	if value.Valid {
		return value.Time.Unix()
	} else {
		return 0
	}
}
