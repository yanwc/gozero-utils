package utils

import (
	"crypto/md5"
	"encoding/hex"
)

// Md5 密码加密
func Md5(value string, salt string) string {
	m5 := md5.New()
	m5.Write([]byte(value))
	m5.Write([]byte(string(salt)))
	st := m5.Sum(nil)
	return hex.EncodeToString(st)
}
