package jsonx

import (
	"encoding/json"
)

func StringToSlice[T any](str string) ([]T, error) {
	var rev []T
	err := json.Unmarshal([]byte(str), &rev)
	if err != nil {
		return nil, err
	} else {
		return rev, nil
	}
}

func StringToSliceOrNil[T any](str string) []T {
	r, err := StringToSlice[T](str)
	if err != nil {
		return make([]T, 0)
	}

	return r
}
