package jwtx

import "testing"

func TestNewJwtTokenData(t *testing.T) {
	d, err := NewJwtTokenData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJteS1hdWQiLCJkYXRhIjp7InVzZXJJZCI6MSwiYWNjZXNzVG9rZW4iOiJhc2RmYXNkZmFzZGYiLCJ0ZW5hbnRJZCI6MX0sImV4cCI6MTczMDQ3MDMzNiwiaXNzIjoibXktaXNzIiwic3ViIjoiYmFja2VuZCJ9.PCAgrytjwWOeCovYnPWRPHMBFU-0kw4IM1xW8Lt5Woc")
	if err != nil {
		t.Error(err)
	}

	if d.TenantId != 1 {
		t.Fail()
	}
}
