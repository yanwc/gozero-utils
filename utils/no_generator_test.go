package utils

import (
	"context"
	"fmt"
	"testing"

	"github.com/redis/go-redis/v9"
)

func TestGenerateNo(t *testing.T) {
	client := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "123456",
		DB:       1,
	})
	for i := 0; i < 100; i++ {
		no, err := GenerateNo(context.Background(), client, "U")
		if err != nil {
			t.Fail()
		}
		fmt.Println(no)
	}
}
