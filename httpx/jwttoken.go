package httpx

import (
	"context"
	"net/http"

	"gitee.com/yanwc/gozero-utils/errx"
	"gitee.com/yanwc/gozero-utils/jwtx"
	"github.com/duke-git/lancet/v2/validator"
)

// 设置请求token的上下文
func WithJwtTokenContext(ctx context.Context, r *http.Request, secret string) (*http.Request, string, *errx.Error) {
	token, err := GetTokenFromRequest(r)
	if err != nil {
		return nil, "", err
	}

	if !validator.IsJWT(token) {
		return r, "", errx.New(errx.JWT_TOKEN_FORMAT)
	}

	var tokenData jwtx.JwtTokenData
	if err := jwtx.ParseToken(token, secret, &tokenData); err != nil {
		return nil, "", err
	}

	r = r.WithContext(context.WithValue(r.Context(), JwtTokenDataKey, tokenData))
	return r, "", nil
}

// 设置JwtTokenData
func WithTokenDataContext(ctx context.Context, r *http.Request, data jwtx.JwtTokenData) (*http.Request, *errx.Error) {
	return r.WithContext(context.WithValue(r.Context(), JwtTokenDataKey, data)), nil
}

// 从ctx获取用户请求内容
func GetJwtTokenDataFromCtx(ctx context.Context) (*jwtx.JwtTokenData, *errx.Error) {
	v := ctx.Value(JwtTokenDataKey)
	if v, ok := v.(jwtx.JwtTokenData); ok {
		return &v, nil
	} else {
		return nil, errx.New(errx.TOKEN_IS_REQUIRED)
	}
}
