package utils

import (
	"fmt"
	"strconv"
	"testing"
)

type (
	T int
	V string
)

func TestArrayTo(t *testing.T) {
	at := []T{1, 2, 3, 4, 5, 65}

	rt:=ArrayConverto[T, V](at, func(t T) V {
		return V(strconv.Itoa(int(t)))
	})

	fmt.Printf("%+v\n",rt)
}
