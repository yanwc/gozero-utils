package jwtx

import (
	"fmt"
	"testing"
)

type UserInfo struct {
	Name    string
	Age     int
	Address string
}

const secret = "secret12345"

func TestNewtokenAndParseToken(t *testing.T) {
	tt, err := NewToken(JwtTokenData{
		TenantId:    1,
		UserId:      1,
		AccessToken: "asdfasdfasdf",
		UserType: Admin,
	}, secret, WithAudOption("my-aud"), WithExpOption(int64(60*60*8)), WithSubOption("backend"), WithIssOption("my-iss"))
	if err != nil {
		panic(err)
	}

	out := JwtTokenData{}
	err = ParseToken(tt, secret, &out)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%#v\r\n", out)
	fmt.Println(tt)
}
