package fileconfig

type Redis struct {
	Host string
	Pass string
	DB   int
}
