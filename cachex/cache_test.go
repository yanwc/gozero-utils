package cachex

import (
	"context"
	"fmt"
	"testing"

	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/redis"
)

type Studnet struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func TestNew(t *testing.T) {
	cs := cache.CacheConf{cache.NodeConf{
		RedisConf: redis.RedisConf{
			Host: "127.0.0.1:6379",
			Pass: "123456",
		},
		Weight: 1,
	}}
	cache := NewCache(cs)
	err := cache.SetCtx(context.Background(), "test", Studnet{
		Name: "ok",
		Age:  100,
	})
	if err != nil {
		fmt.Println("ok")
	}

	cache1 := NewCache(cs)
	err = cache1.SetCtx(context.Background(), "test", Studnet{
		Name: "ok",
		Age:  100,
	})
	if err != nil {
		fmt.Println("ok")
	}
}
