package typex

func ValueToPtr[T ~int | ~int8 | ~int16 | ~int32 | ~int64 | string | bool](t T) *T {
	return &t
}
