package errx

var message map[Code]string

func init() {
	message = make(map[Code]string)
	message[OK] = "SUCCESS"
	message[SERVER] = "服务器开小差啦,稍后再来试一试"
	message[REUQEST_PARAM] = "参数错误"
	message[TOKEN_EXPIRE] = "token失效,请重新登陆"
	message[TOKEN_GENERATE] = "生成token失败"
	message[TOKEN_PARSER] = "解析错误"
	message[DB] = "数据库繁忙,请稍后再试"
	message[DB_INSERT] = "创建失败"
	message[DB_ALREADY_EXISTS] = "已经存在"
	message[DB_QUERY] = "查询失败"
	message[DB_NOT_FOUND] = "记录不存在"
	message[PARAM_REQUIRED] = "缺少参数"
	message[DB_UPDATE] = "更新失败"
	message[SIGNED_IN_ELSE_WHERE] = "在其他地方登录"
	message[LOGIN_TIMEOUT] = "登录超时"
	message[CACHE_ERROR] = "缓存错误"
	message[CACHE_EXPIRE] = "缓存已过期"
	message[JSON_MARSHAL] = "JSON 序列化失败"
	message[SQL_BUILD] = "sql构造错误"
	message[REQUIRED_TOKEN] = "必须提供认证令牌"
	message[CAPTCHA_ERROR] = "验证码错误"
	message[RECORD_DISABLED] = "已禁用"
	message[CACHE_NOT_FOUND] = "缓存不存在"
	message[DB_DELETE] = "删除失败"
	message[UNAUTHENTICATION] = "未认证"
	message[UNAUTHORIZED] = "未授权"
	message[BIZ] = "业务失败"
	message[CONFIG_RANGE_FORMAT] = "系统配置范围格式错误"
	message[NOT_IMPLEMENTED] = "功能未实现"
	message[NOT_SUPPORTED] = "功能不支持"
	message[JWT_TOKEN_FORMAT] = "Jwt Token 格式不正确"
	message[TOKEN_IS_REQUIRED] = "缺少验证Token"
	message[USER_CONTEXT_NOT_EXIST] = "用户上下文不存在"
	message[UUID_GENERATE] = "UUID 生成失败"
	message[PASSWORD] = "密码错误"
	message[PASSWORD_INCONSISTENT] = "两次密码不一致"
	message[PASSWORD_NEW_OLD_CONSISTENT] = "新旧密码一致"
	message[PASSWORD_USER] = "用户名或密码错误"
	message[JSON_UNMARSHAL] = "反序列化失败"
	message[STRING_CONVERT_NUMBER] = "非法字符串转数字"
	message[TOKEN_VALID] = "Token 签名验证不通过"
}

// SetErrorMessageWithError 全局错误码消息
func SetErrorMessageWithError(codeErrs ...*Error) {
	for _, codeErr := range codeErrs {
		message[codeErr.code] = codeErr.msg
	}
}

// SetErrorMessgeWithMap
func SetErrorMessgeWithMap(data map[Code]string) {
	for k, v := range data {
		message[k] = v
	}
}

// IsCodeErr 是否自定义错误
func IsCodeErr(code Code) bool {
	if _, ok := message[code]; ok {
		return true
	} else {
		return false
	}
}
