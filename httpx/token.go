package httpx

import (
	"context"
	"fmt"
	"net/http"
	"regexp"

	"gitee.com/yanwc/gozero-utils/api"
	"gitee.com/yanwc/gozero-utils/errx"
	"gitee.com/yanwc/gozero-utils/sessionx"
	"github.com/zeromicro/go-zero/rest/httpx"
	oteltrace "go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/metadata"
)

type (
	RpcTokenKey string
)

func NewRpcTokenKey() RpcTokenKey {
	return RpcTokenKey("token")
}

type UserContextKey string

const (
	JwtTokenDataKey     = UserContextKey("JwtTokenData")
	AdminUserContextKey = UserContextKey("UserContext")
)

// 读取Request Header Authorization
func GetTokenFromRequest(r *http.Request) (string, *errx.Error) {
	auth := r.Header.Get("Authorization")
	pattern := "Bearer +(.+)"
	exp := regexp.MustCompile(pattern)
	if len(auth) > 0 {
		if !exp.Match([]byte(auth)) {
			return "", errx.New(errx.TOKEN_PARSER)
		}
	} else {
		return "", nil
	}

	values := exp.FindSubmatch([]byte(auth))
	return string(values[1]), nil
}

// 透传给Rpc authorization 认证token
func AdminHandle(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req, err := withRpcTokenContext(r)
		if err != nil {
			api.HttpResult(r, w, nil, errx.New(errx.TOKEN_PARSER))
			return
		}
		next(w, req)
	}
}

// 透传给Rpc authorization 认证token
func withRpcTokenContext(r *http.Request) (*http.Request, *errx.Error) {
	token, err := GetTokenFromRequest(r)
	if err != nil {
		return r, err
	}

	md := metadata.MD{
		"authorization": []string{token},
	}

	metaCtx := context.WithValue(r.Context(), NewRpcTokenKey(), token)
	newCtx := metadata.NewOutgoingContext(metaCtx, md)
	return r.WithContext(newCtx), nil
}

const (
	REFESH_TOKEN_EXPIRED_AT = "refresh-token-expired-at"
	TOKEN_EXPIRED_AT        = "access-token-expired-at"
)

type CanAccessResp struct {
	Access                bool
	TokenExpiredAt        int64
	RefreshTokenExpiredAt int64
	AccessToken           string
	TenantId              int64
	TenantCode            string
	UserId                int64
	UserCode              string
	UserName              string
	Mobile                string
	Mail                  string
	JwtToken              string
	PermissionGroup       string // 权限组
	PermissionCode        string // 权限码
	TraceId               string
	SpanId                string
}

// 检查是否可以访问资源
func CanAccess(next http.HandlerFunc, check func(r *http.Request) (*CanAccessResp, error)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if rev, err := check(r); err != nil {
			api.HttpResult(r, w, nil, err)
		} else if rev.Access {
			if rev.TokenExpiredAt > 0 {
				w.Header().Set(REFESH_TOKEN_EXPIRED_AT, fmt.Sprintf("%d", rev.RefreshTokenExpiredAt))
				w.Header().Set(TOKEN_EXPIRED_AT, fmt.Sprintf("%d", rev.TokenExpiredAt))
			}

			spanCtx := oteltrace.SpanFromContext(r.Context()).SpanContext()
			rev.TraceId = spanCtx.TraceID().String()
			rev.SpanId = spanCtx.SpanID().String()
			next(w, r.WithContext(context.WithValue(r.Context(), AdminUserContextKey, rev)))
		} else {
			httpx.WriteJson(w, http.StatusForbidden, api.Fail(errx.UNAUTHORIZED, "未授权"))
		}
	}
}

// 获取后台用户上下文
func GetUserContextFromCtx(ctx context.Context) (*sessionx.UserContext, *errx.Error) {
	v := ctx.Value(AdminUserContextKey)
	if v, ok := v.(*CanAccessResp); ok {
		return &sessionx.UserContext{
			UserId:                v.UserId,
			UserCode:              v.UserCode,
			AccessToken:           v.AccessToken,
			JwtToken:              v.JwtToken,
			TenantId:              v.TenantId,
			AccessTokenExpiredAt:  v.TokenExpiredAt,
			RefreshTokenExpiredAt: v.RefreshTokenExpiredAt,
			PermissionGroup:       v.PermissionGroup,
			PermissionCode:        v.PermissionCode,
			TraceId:               v.TraceId,
			SpanId:                v.SpanId,
			Mobile:                v.Mobile,
			UserName:              v.UserName,
			Mail:                  v.Mail,
			TenantCode:            v.TenantCode,
		}, nil
	} else {
		return nil, errx.New(errx.USER_CONTEXT_NOT_EXIST)
	}
}
