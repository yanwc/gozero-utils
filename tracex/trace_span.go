package tracex

import (
	"context"

	"github.com/zeromicro/go-zero/core/trace"
	"go.opentelemetry.io/otel"
	oteltrace "go.opentelemetry.io/otel/trace"
)

func TraceSpan(serviceName string, spanName string, ctx context.Context) (context.Context, oteltrace.Span) {
	tracer := otel.GetTracerProvider().Tracer(trace.TraceName)
	spanCtx, span := tracer.Start(
		ctx,
		"",
		oteltrace.WithSpanKind(oteltrace.SpanKindInternal),
	)
	return spanCtx, span
}
